
#include <stdbool.h>
#include <stm8s.h>
#include "main.h"
#include "milis.h"
//#include "TM1637Display.h"
#include "max7219.h"

#define OUTPUT_1 GPIOD, GPIO_PIN_6      //red-black
#define OUTPUT_2 GPIOD, GPIO_PIN_5      //black-red
#define OUTPUT_3 GPIOD, GPIO_PIN_2      //power control

#define DIN_PORT GPIOB
#define DIN_PIN GPIO_PIN_4
#define CS_PORT GPIOB
#define CS_PIN GPIO_PIN_3
#define CLK_PORT GPIOB
#define CLK_PIN GPIO_PIN_2


#define BTN_1_PORT GPIOF
#define BTN_1_PIN GPIO_PIN_5
#define BTN_2_PORT GPIOF
#define BTN_2_PIN GPIO_PIN_6
#define BTN_3_PORT GPIOF
#define BTN_3_PIN GPIO_PIN_7

#define BTN_1 GPIOF, GPIO_PIN_5
#define BTN_2 GPIOF, GPIO_PIN_6
#define BTN_3 GPIOF, GPIO_PIN_7

void init(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);      // taktovani MCU na 16MHz

    GPIO_Init(OUTPUT_1, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(OUTPUT_2, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(OUTPUT_3, GPIO_MODE_OUT_PP_LOW_SLOW);
    
    GPIO_Init(DIN_PORT, DIN_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(CS_PORT, CS_PIN, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(CLK_PORT, CLK_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);

    GPIO_Init(BTN_1, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(BTN_2, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(BTN_3, GPIO_MODE_IN_PU_NO_IT);

    // nastaveni jasu

    init_milis();
}


void display(uint8_t address, uint8_t data) {
    uint8_t mask;
    LOW(CS); // začátek přenosu

    mask = 0b10000000;
    while (mask) {
        if (address & mask) {
            HIGH(DIN);
        } else {
            LOW(DIN);
        }
        HIGH(CLK);
        mask = mask >> 1;
        LOW(CLK);
    }
    mask = 0b10000000;
    while (mask) {
        if (data & mask) {
            HIGH(DIN);
        } else {
            LOW(DIN);
        }
        HIGH(CLK);
        mask = mask >> 1;
        LOW(CLK);
    }

    HIGH(CS); // konec přenosu
}


int main(void)
{


    uint32_t time = 0;
    uint32_t time_a=0;
    uint32_t time_button=0;
    uint8_t minutes_c=0;        //clock
    uint8_t hours_c=0;
    uint8_t minutes_a=11;        //alarm
    uint8_t hours_a=11;
    bool alarming = true;

    bool bol = false;           //testing purposes
    GPIO_WriteHigh(OUTPUT_1);
    GPIO_WriteHigh(OUTPUT_2);

    init();
    time = milis();
    time_a = milis();
    display(DECODE_MODE, 0b11111111);
    display(SCAN_LIMIT, 7);
    display(INTENSITY, 1);
    display(DISPLAY_TEST, DISPLAY_TEST_OFF);
    display(SHUTDOWN, SHUTDOWN_ON);
    display(DIGIT4, minutes_c%10);
    display(DIGIT5, minutes_c/10);
    display(DIGIT6, hours_c%10);
    display(DIGIT7, hours_c/10);

    //
    while (1) {
        //kontroluje, jestli už má alarm běžet
        if (minutes_c==minutes_a && hours_c==hours_a && alarming==false) {
            alarming=true;
        }
        //////////////////////////////////////

        //cyklus alarmu
        //kontroluje, jeslti je čas na hodinách roven nastavenému budícímu času
        if (alarming==true) {
            if (milis()-time_a>=500) {
                GPIO_WriteLow(OUTPUT_3);

            }
            if (milis()-time_a>=1000) {
                time_a=milis();
                bol = !bol;

                if (bol)
                {
                    GPIO_WriteLow(OUTPUT_1);
                    GPIO_WriteLow(OUTPUT_2);
                }
                else 
                {
                    GPIO_WriteHigh(OUTPUT_1);
                    GPIO_WriteHigh(OUTPUT_2);
                }
                GPIO_WriteHigh(OUTPUT_3);
            }
        }
        /////////////////////////////////////////////////////////////////////////

        //spustí se, když uběhna minuta (teď vteřina) od posledního spuštění této podmínky
        //pokud ano, přičte minutu k současnému času
        //taky se tu zařizuje zobrazení počtu minut na sedmisegmentech
        if (milis() - time > 1000000 /1000) {
            time=milis();
            minutes_c+=1;
            display(DIGIT4, minutes_c%10);
            if (minutes_c!=0) {
                display(DIGIT5, minutes_c/10);
            }
            else {
                display(DIGIT5, 0);
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////

        //spustí se, je na hodinách více jak 60 minut, pak od odečte 60 od počtu minut a přičte 1 k počtu hodinám
        //taktéž řeší zobrazení počtu hodin na hodinách
        if (minutes_c>=60) {
            minutes_c-=60;
            hours_c+=1;
            if (hours_c!=0) {
                display(DIGIT6, hours_c%10);
                }
            else {
                display(DIGIT6, 0);
            }
            if (hours_c/10!=0) {
                display(DIGIT7, hours_c/10);
                }
            else {
                display(DIGIT7, 0);
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////

        //okntroluje, jestli je počet hodin rovno nebo více jak 24, pokud ano, odečte od poČtu hodin 24
        if (hours_c>=24) {
            hours_c-=24;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////

        //na konci každého cyklu alarmu je sepnutí propojení mezi zdrojem napětí a motorem - tato podmínka ho rozepne, pokud už alarm nemá běžet
        //tato podmínaky taky vypíná funkci alarmu
        if (minutes_c!=minutes_a && alarming==true) {
            GPIO_WriteLow(OUTPUT_3);
            alarming=false;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //čeká na to, jestli je nějaké tlačítko spuštěno (může se tak stát maximálně třikrát za vteřinu)
        //pokud je nějaké tlačítko spuštěno, přičte korespondující hodnotu k počtu minut či hodin na alarmu:
        // tlačítko 1 - +1 minuta, tlačítko 2 - +10 minut, tlačítko 3 - +1 hodina
        if (milis()-time_button>=333) {
            time_button=milis();
            if(PUSH(BTN_1 ) ) {
                minutes_a+=1;
            }
            if(PUSH(BTN_2 ) ) {
                minutes_a+=10;
            }
            if(PUSH(BTN_3 ) ) {
                hours_a+=1;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////

        //kontroluje, jesli je na počtu hodin či minut na alarmu víc, než by mělo být
        //podobně řešeno i u hodin a minut na hodinách
        if (minutes_a>=60) {
            hours_a+=1;
            minutes_a-=60;
        }
        if (hours_a>=24) {
            hours_a-=24;
        }
        //////////////////////////////////////////////////////////////////////////////

        //zobrazuje čas na alarmu na semidegmentu
        display(DIGIT0, minutes_a%10);
        display(DIGIT1, minutes_a/10);
        display(DIGIT2, hours_a%10);
        display(DIGIT3, hours_a/10);
        //
    }
}

#include "__assert__.h"